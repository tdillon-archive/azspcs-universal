export default class T {
    private _bases: number[];
    private _e: bigint;
    private _target: bigint;
    private static _powerCache: bigint[] = [];
    private static _sumOfPowersCache: bigint[] = [];

    constructor(private _problem: number, private _s: number) {
        this._target = T.power(this._s, this._problem);
        this._bases = [];
        this._e = this._target;
    }

    /**
     * Factory method to return a solution from an AZSPCS solution string.
     * @param {string} s String in format of an AZSPCS solution (i.e., s^n=>{a1,a2,...,ak}) e.g., 3^16=>{2,1}
     */
    public static FromSolutionString(s: string) {
        let tempS = +s.substring(0, s.indexOf('^'));
        let tempN = +s.substring(s.indexOf('^') + 1, s.indexOf('='))
        let tempBases = s.substring(s.indexOf('{') + 1, s.length - 1).split(',').map(stringBase => +stringBase);
        let solution = new T(tempN, tempS);
        tempBases.forEach(b => { solution.addBase(b) });
        return solution;
    }

    public get isValid(): boolean {
        return this._bases.length >= 2;
    }

    public get E(): bigint {
        return this._e;
    }

    public get rawScore(): number {
        return Math.log(parseFloat((this.absE).toString()) + 1) + 1;
    }

    public get absE(): bigint {
        return this._e < 0 ? - this._e : this._e;
    }

    static sumOfPowers(num: number, pow: number): bigint {
        if (!T._sumOfPowersCache[num]) {
            T._sumOfPowersCache[num] = num < 1 ? 0n : T.power(num, pow) + T.sumOfPowers(num - 1, pow);
        }

        return T._sumOfPowersCache[num];
    }

    static power(num: number, pow: number): bigint {
        if (!T._powerCache[num]) {
            const bignum = BigInt(num);
            let result = bignum;

            while (pow > 1) {
                result *= bignum;
                --pow;
            }

            T._powerCache[num] = result;
        }

        return T._powerCache[num];
    }


    /**
     * Add the given {base} to the solution if it is an improvement.
     *
     * @param {number} base - todo what is this
     * @returns {boolean} true if the base was added (improving the score), false otherwise.
     */
    addBase(base: number): boolean {
        let origE = this._e;
        let tempE = this._e - T.power(base, this._problem);
        if ((tempE < 0 ? -tempE : tempE) < (origE < 0 ? -origE : origE)) {
            this._e = tempE;
            this._bases.push(base);
            return true;
        } else {
            return false;
        }
    }

    toString() {
        return `${this._s}^${this._problem}=>{${this._bases.join(',')}}`;
    }
}
