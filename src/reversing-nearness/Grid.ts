export default class Grid {
    private originalAllDists: Array<number>;
    private gridLabels: Array<string> = [];
    private grid: Array<number> = [];

    private cellLookup: Array<number> = [];
    private currentAllDists: Array<number> = [];

    private numCells: number;
    private numPairs: number;

    private isDirty = true;
    private _score: number;

    private lowerBounds = [0, 0, 10, 72, 816, 3800, 16902, 52528, 155840, 381672, 902550, 1883244, 3813912, 7103408, 12958148, 22225500, 37474816, 60291180, 95730984, 146469252, 221736200, 325763172, 474261920, 673706892, 949783680, 1311600000, 1799572164, 2425939956, 3252444776, 4294801980, 5643997650];

    constructor(public readonly n = 5) {
        this.numCells = n * n;
        this.numPairs = n * n * (n * n - 1) / 2;

        for (let i = 0; i < this.numCells; ++i) {
            this.cellLookup.push(i);
            this.grid.push(i);
            this.gridLabels.push(`${String.fromCharCode(i % this.n + 65)}${String.fromCharCode(Math.floor(i / this.n) + 65)}`);
        }

        this.originalAllDists = this.calcAllDists(cell => cell);
        this.isDirty = true;
    }

    public getGreedyBest() {
        let best: number;
        let bestSwap: number;
        let foundBest: boolean;
        let absoluteBest = Number.MAX_SAFE_INTEGER;
        let absoluteBestString: string;


        do {
            best = Number.MAX_SAFE_INTEGER
            foundBest = false;

            for (let i = 0; i < this.numCells; ++i) {
                bestSwap = i;
                for (let j = 0; j < this.numCells; ++j) {
                    this.swap(i, j);
                    if (this.score < best) {
                        best = this.score;
                        bestSwap = j;

                        if (best < absoluteBest) {
                            foundBest = true;
                            absoluteBest = best;
                            absoluteBestString = this.toString();
                        }
                    }
                    this.swap(i, j);
                }
                this.swap(i, bestSwap);
            }
        } while (foundBest);

        this.loadFromGridString(absoluteBestString);
    }

    /**
     * For a given pair (i,j) what is the index into originalAllDists or currentAllDists?
     */
    private getPairIndex(i: number, j: number) {
        // TODO: can this be cached?
        let iPrime = Math.min(i, j);
        let jPrime = Math.max(i, j);
        return iPrime * (this.numCells - iPrime * 0.5 - 0.5) + (jPrime - iPrime) - 1;
    }

    /**
     * Swap keeps score and currentAllDists up-to-date.
     */
    public swap(i: number, j: number) {
        let iLocation = this.cellLookup[i];
        let jLocation = this.cellLookup[j];
        let temp = this.grid[iLocation];
        this.grid[iLocation] = this.grid[jLocation];
        this.grid[jLocation] = temp;
        this.cellLookup[i] = jLocation;
        this.cellLookup[j] = iLocation;

        let ORIG_SCORE = 0;
        let NEW_SCORE = 0;

        for (let k = 0; k < this.numCells; ++k) {
            if (k !== i) {
                let cadIdx = this.getPairIndex(i, k)
                ORIG_SCORE += this.originalAllDists[cadIdx] * this.currentAllDists[cadIdx];
                this.currentAllDists[cadIdx] = this.getDistBetweenPair(i, k, cell => this.cellLookup[cell]);
                NEW_SCORE += this.originalAllDists[cadIdx] * this.currentAllDists[cadIdx];
            }
            if (k !== j && k !== i) {
                let cadIdx = this.getPairIndex(j, k)
                ORIG_SCORE += this.originalAllDists[cadIdx] * this.currentAllDists[cadIdx];
                this.currentAllDists[cadIdx] = this.getDistBetweenPair(j, k, cell => this.cellLookup[cell]);
                NEW_SCORE += this.originalAllDists[cadIdx] * this.currentAllDists[cadIdx];
            }
        }

        this._score = this._score - ORIG_SCORE + NEW_SCORE;
    }

    private loadFromGridString(grid: string) {
        let tempGrid: number[] = JSON.parse(grid);

        if (tempGrid.length !== this.numCells) {
            throw 'ERROR! loadFromGridString'
        }

        tempGrid.forEach((val, idx) => {
            this.cellLookup[val] = idx;
            this.grid[idx] = val;
        });

        this.isDirty = true;
    }

    /**
     * @param {string} input '[3,2,4,...,0,1]'
     */
    public static fromStringOfArrayOfGridPositions(input: string): Grid {
        let g = new Grid(Math.sqrt(JSON.parse(input).length));
        g.loadFromGridString(input);
        return g;
    }

    /**
     * @param grid ['AA', 'DE', ..., 'BC']
     */
    public static fromArrayOfGridLabels(grid: Array<string>) {
        let g = new Grid(Math.sqrt(grid.length));

        grid.forEach((val, idx) => {
            let row = val.charCodeAt(1) - 65;
            let col = val.charCodeAt(0) - 65;
            let cellNum = row * g.n + col;
            g.cellLookup[cellNum] = idx;
            g.grid[idx] = cellNum;
        });

        return g;
    }

    public randomize(num = this.numCells) {
        for (let i = 0; i < num; ++i) {
            this.swap(Math.floor(Math.random() * this.numCells), Math.floor(Math.random() * this.numCells));
        }
    }

    private getDistBetweenPair(cellA: number, cellB: number, lookupFunction: { (cell: number): number }): number {
        const n = this.n;

        let cellAPosition = lookupFunction(cellA);
        let cellBPosition = lookupFunction(cellB);
        let rowA = Math.floor(cellAPosition / n);
        let colA = cellAPosition % n;
        let rowB = Math.floor(cellBPosition / n);
        let colB = cellBPosition % n;

        let opt1 = Math.pow(rowB - rowA, 2) + Math.pow(n - Math.abs(colB - colA), 2);
        let opt2 = Math.pow(rowB - rowA, 2) + Math.pow(colB - colA, 2);
        let opt3 = Math.pow(n - Math.abs(rowB - rowA), 2) + Math.pow(colB - colA, 2);
        let opt4 = Math.pow(n - Math.abs(rowB - rowA), 2) + Math.pow(n - Math.abs(colB - colA), 2);

        return Math.min(opt1, opt2, opt3, opt4);
    }

    private calcAllDists(lookupFunction: { (cell: number): number }) {
        let allDists: Array<number> = [];
        const n = this.n;

        for (let cellA = 0; cellA < this.numCells - 1; ++cellA) {
            for (let cellB = cellA + 1; cellB < this.numCells; ++cellB) {
                allDists.push(this.getDistBetweenPair(cellA, cellB, lookupFunction));
            }
        }

        return allDists;
    }

    public get score(): number {
        if (this.isDirty) {
            this.currentAllDists = this.calcAllDists(cell => this.cellLookup[cell]);
            this._score = this.originalAllDists.reduce((prev, curr, idx) => prev + curr * this.currentAllDists[idx], 0) - this.lowerBounds[this.n];
            this.isDirty = false;
        }

        return this._score;
    }

    /**
     * @param fancy?
     * .toString(true)
     * ┏━━━━━━━━━━━━━━━━━━┓
     * ┃ n:5       s:1400 ┃
     * ┣━━━━━━━━━━━━━━━━━━┫
     * (AE, DB, AC, ED, CA),
     * (CD, BD, BA, CC, BB),
     * (AA, EC, DD, AD, BE),
     * (DE, DA, EA, DC, CB),
     * (EB, AB, EE, CE, BC)
     * ┗━━━━━━━━━━━━━━━━━━┛
     *
     * .toString(false)
     * (AE, DB, AC, ED, CA),
     * (CD, BD, BA, CC, BB),
     * (AA, EC, DD, AD, BE),
     * (DE, DA, EA, DC, CB),
     * (EB, AB, EE, CE, BC)
     *
     * .toString()
     * [20,8,10,19,2,17,16,1,12,6,0,14,18,15,21,23,3,4,13,7,9,5,24,22,11]
     *
     */
    toString(fancy?: boolean) {
        if (typeof fancy === 'boolean') {
            const maxWidth = 4 * (this.n) - 4;  // Distance between () for each row of the grid.
            const padding = 2;

            let solution: Array<string> = fancy ? [
                // ┏━━━━━━━━━━━┓
                '┏',
                '━'.repeat(maxWidth + padding),
                '┓',
                '\n',
                // ┃ n:3  s:45 ┃
                '┃ n:',
                this.n.toString(),
                ' '.repeat(maxWidth - 4 - this.n.toString().length - this.score.toString().length),
                's:',
                this.score.toString(),
                ' ┃',
                '\n',
                // ┣━━━━━━━━━━━┫
                '┣',
                '━'.repeat(maxWidth + padding),
                '┫',
                '\n'] : []

            for (let row = 0; row < this.n; ++row) {
                solution.push('(');

                for (let col = 0; col < this.n; ++col) {
                    solution.push(this.gridLabels[this.grid[row * this.n + col]]);
                    solution.push(', ');
                }

                solution.pop(); // Remove last comma and space.
                solution.push(')');
                solution.push(',\n');
            }

            solution.pop();  // Remove last comma.

            if (fancy) {
                // ┗━━━━━━━━━━━┛
                solution.push(
                    '\n',
                    '┗',
                    '━'.repeat(maxWidth + padding),
                    '┛');
            }

            return solution.join('');
        } else {
            return JSON.stringify(this.grid);
        }
    }

}