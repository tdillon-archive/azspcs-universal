import { SolverLayers } from "./SolverLayers.js";
import { UniqueHexPermutation } from "./UniqueHexPermutation.js";
import { BrowserMessageEvent, WorkerMessageData } from "../main/browser";

const CONTEST_NAME = 'hexagonal-neighbors';

const ctx: Worker = self as any; // HACK from https://github.com/Microsoft/TypeScript/issues/20595#issuecomment-390359040

const knownBests = [39, 87, 147, 227, 325, 442, 576, 728, 902, 1090, 1292, 1517, 1761, 2023, 2297, 2602, 2910, 3240, 3595, 3961, 4346, 4755, 5175, 5613, 6077];

// Send 'working' message back to browser, signifies that browser supports modules in web workers.
ctx.postMessage(<WorkerMessageData>{
    type: 'WORKING',
    message: `[WORKER] worker for ${CONTEST_NAME} is functional`,
    name: CONTEST_NAME,
    problems: Array.from({ length: 25 }, (v, i) => {
        return { name: (i + 3).toString(), knownBest: knownBests[i] }
    })
});

onmessage = (e: BrowserMessageEvent) => {
    const browserMessageData = e.data;
    ctx.postMessage(<WorkerMessageData>{
        type: 'LOG',
        message: `[WORKER] ${CONTEST_NAME} received ${browserMessageData.type} message`
    });

    switch (browserMessageData.type) {
        case 'SOLVE':
            ctx.postMessage(<WorkerMessageData>{
                type: 'LOG',
                message: `[WORKER] ${CONTEST_NAME} received request to solve problem ${browserMessageData.problem} with best ${browserMessageData.myBest}`
            });

            const bestScore = browserMessageData.myBest || Number.MIN_SAFE_INTEGER;
            const solver = new SolverLayers(+browserMessageData.problem, bestScore, new UniqueHexPermutation().hexPerms);

            solver.onNewBest = (solution: string, score: number) => {

                let x: WorkerMessageData = {
                    type: 'SOLUTION',
                    message: `[WORKER] New solution for ${CONTEST_NAME} problem ${browserMessageData.problem} with score ${score}`,
                    contest: CONTEST_NAME,
                    problem: browserMessageData.problem,
                    solution,
                    score,
                    comparator: '>'
                };
                ctx.postMessage(x);

            };
            solver.solve();
            break;
        default:
            ctx.postMessage(<WorkerMessageData>{
                type: 'LOG',
                message: `[WORKER] [ERROR] Bad message type: ${browserMessageData.type}`
            });
            break;
    }
};
