import { APISaveResponse, APISaveBodyData, APIGetSolutionResponse, APIGetProblemsResponse } from "./server";
import { FirestoreError } from "./DAO";
import LogEvent from "./LogEvent.js";

//#region Interfaces and Types

/** This type is used for TODO ... */
type BrowserMessageData = { type: 'SOLVE'; problem: string; myBest: number; }

/** This interface is used for TODO ... */
export interface BrowserMessageEvent extends MessageEvent { data: BrowserMessageData }

/** This type is used for TODO ... */
export type WorkerMessageData = {
    type: 'WORKING';
    message: string;
    name: string;
    problems: {
        name: string;
        knownBest: number;
    }[]
} | {
    type: 'LOG';
    message: string;
} | {
    type: 'SOLUTION';
    message: string;
    contest: string;
    problem: string;
    solution: string;
    score: number;
    comparator: '<' | '>';
}

/** This interface is used for TODO ... */
interface WorkerMessageEvent extends MessageEvent { data: WorkerMessageData; }

/** This interface is used for TODO ... */
type VueProblem = {
    name: string,
    score: number,
    solution: string,
    best: number,
    percentage: number,
    lastUpdate: number,
    workers: Worker[]
}

/** This interface is used for TODO ... */
type VueContest = {
    name: string;
    hide: boolean;
    problems: VueProblem[];
    allSolutions: string
}

/** This interface is used for TODO ... */
type VueDataStructure = {
    supportsWorkerModules: boolean,
    contests: VueContest[],
    logs: LogEvent[]
}

//#endregion

/** Data object used by Vue. */
const data: VueDataStructure = { supportsWorkerModules: false, contests: [], logs: [] }

const contestNames = ['reversing-nearness', 'hexagonal-neighbors'];

function getWorker(url: string) {
    let w = new Worker(url, { type: "module" });

    w.onmessage = (e: WorkerMessageEvent) => {
        const workerMessageData = e.data;

        switch (workerMessageData.type) {
            case 'LOG':
                data.logs.unshift(new LogEvent(workerMessageData.message));
                break;
            case 'WORKING':
                data.logs.unshift(new LogEvent(workerMessageData.message));
                data.supportsWorkerModules = true;

                if (data.contests.some(c => c.name === workerMessageData.name)) {
                    data.logs.unshift(new LogEvent(`[BROWSER] Contest ${workerMessageData.name} already added to Vue data object`));
                } else {
                    data.logs.unshift(new LogEvent('[BROWSER] New contest information received, adding to Vue data object'));
                    data.contests.push({
                        name: workerMessageData.name,
                        hide: true,
                        problems: workerMessageData.problems.map(p => {
                            return {
                                name: p.name,
                                score: null,
                                solution: null,
                                best: p.knownBest,
                                get percentage() { return +(100 * (1 - Math.abs(this.score - this.best) / this.best)).toFixed(2) },
                                lastUpdate: null,
                                workers: []
                            }
                        }),
                        get allSolutions() {
                            return this.problems.reduce((accumulator: string, problem: VueProblem) =>
                                problem.solution ? `${accumulator + problem.solution};` : accumulator, '')
                                .replace(/;$/, '');
                        }
                    });

                    data.logs.unshift(new LogEvent(`[BROWSER] fetching all problems for ${workerMessageData.name}`));

                    fetch(`${workerMessageData.name}`, {
                        method: 'GET',
                        headers: { "Content-Type": "application/json" }
                    }).then(res => res.json().then((problemsResponse: APIGetProblemsResponse) => {
                        switch (problemsResponse.status) {
                            case 'SUCCESS':
                                data.logs.unshift(new LogEvent(problemsResponse.message));
                                const contest = data.contests.find(c => c.name === workerMessageData.name);
                                problemsResponse.problems.forEach(p => {
                                    const problem = contest.problems.find(prob => prob.name === p.name);
                                    problem.score = p.score;
                                    problem.solution = p.solution;
                                    problem.lastUpdate = p.when;
                                })
                                break;
                            case 'ERROR':
                                data.logs.unshift(new LogEvent(problemsResponse.message));
                                break;
                            default:
                                break;
                        }
                    })).catch((e: FirestoreError) => data.logs.unshift(new LogEvent(`[BROWSER] [ERROR] getSolutions errored: ${JSON.stringify(e)}`)));
                }
                break;
            case 'SOLUTION':
                data.logs.unshift(new LogEvent(workerMessageData.message));
                fetch(`${workerMessageData.contest}/${workerMessageData.problem}`, {
                    method: 'POST',
                    body: JSON.stringify(<APISaveBodyData>{
                        score: workerMessageData.score,
                        solution: workerMessageData.solution,
                        minIsBest: workerMessageData.comparator === "<"
                    }),
                    headers: { "Content-Type": "application/json" }
                }).then(res => res.json().then((r: APISaveResponse) => {
                    switch (r.status) {
                        case 'ERROR':
                            data.logs.unshift(new LogEvent(r.message));
                            break;
                        case 'SUCCESS':
                            data.logs.unshift(new LogEvent(r.message));

                            let p = data.contests.find(c => c.name === r.contest).problems.find(p => p.name === r.problem);
                            p.score = r.score
                            p.solution = r.solution;
                            p.lastUpdate = r.lastUpdate
                            break;
                        default:
                            break;
                    }
                })).catch(() => {
                    data.logs.unshift(new LogEvent('[BROWSER] [ERROR] unexpected error, potentially offline, check console'));
                    console.error('Solution found, but error:', workerMessageData);
                });

                break;
            default:
                break;
        }
    }

    return w;
}

// Spin an idle worker to verify browser support and trigger worker info message.
contestNames.forEach(contestName => getWorker(`${contestName}/worker.js`));

let vueConfig = {
    el: '#app',
    data,
    methods: {
        selectAllText(evt: MouseEvent) {
            (evt.target as HTMLTextAreaElement).select();
        },
        addWorker(evt: MouseEvent) {
            /**
             * This is the add button used to create a new worker for a given contest/problem.
             * Vue adds `contest` and `problem` dataset attributes which represent the index
             * into their corresponding arrays of the Vue data object.
             * These values will be returned as string, but need converted to numbers for use.
             */
            let input = evt.target as HTMLInputElement;

            // TODO explain that this object is just a convience to get typinginfo.
            //      this is the vue object which will contain data at the root.
            let viewData: VueDataStructure = { contests: this.contests, supportsWorkerModules: null, logs: null }
            let contest = viewData.contests[+input.dataset.contest];
            let problem = contest.problems[+input.dataset.problem];

            let w = getWorker(`${contest.name}/worker.js`);

            problem.workers.push(w);

            data.logs.unshift(new LogEvent(`[BROWSER] Add worker for contest ${contest.name} problem ${problem.name}`));

            let bme: BrowserMessageData = {
                type: "SOLVE",
                problem: problem.name,
                myBest: problem.score
            }

            w.postMessage(bme);
        },
        deleteWorker(evt: MouseEvent) {
            /**
             * This is the delete button used to remove a specific worker for a given contest/problem.
             * Vue adds `contest`, `problem`, and `worker` dataset attributes which represent the index
             * into their corresponding arrays of the Vue data object.
             * These values will be returned as string, but need converted to numbers for use.
             */
            let input = evt.target as HTMLInputElement;

            // TODO explain that this object is just a convience to get typinginfo.
            //      this is the vue object which will contain data at the root.
            let viewData: VueDataStructure = { contests: this.contests, supportsWorkerModules: null, logs: null }
            let contest = viewData.contests[+input.dataset.contest];
            let problem = contest.problems[+input.dataset.problem];
            let worker = problem.workers[+input.dataset.worker];

            data.logs.unshift(new LogEvent(`[BROWSER] Terminating contest ${contest.name} problem ${problem.name} worker #${input.dataset.worker}`));

            worker.terminate();

            problem.workers.splice(+input.dataset.worker, 1);
        },
    },
    watch: {
        contests: {
            deep: true,
            handler(contests: VueContest[]) {
                contests.forEach(contest => {
                    const canvas: HTMLCanvasElement = document.querySelector(`#canvas-${contest.name}`);
                    if (!canvas) { return; }
                    const ctx = canvas.getContext('2d');
                    ctx.font = '8px Roboto, sans-serif';
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    let pxPerWidth = canvas.width / (contest.problems.length - 1);
                    let min = contest.problems.reduce((prev, curr) => Math.min(prev, curr.best), Number.MAX_SAFE_INTEGER);
                    let max = contest.problems.reduce((prev, curr) => Math.max(prev, curr.best), Number.MIN_SAFE_INTEGER);
                    let pxPerHeight = canvas.height / (max - min);
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    contest.problems.forEach((problem, idx) => {

                        // Known best
                        ctx.beginPath();
                        let x = idx * pxPerWidth;
                        let y = pxPerHeight * (max - problem.best);
                        ctx.arc(x, y, 8, 0, Math.PI * 2);
                        ctx.fillStyle = '#00CC66'
                        ctx.fill();
                        ctx.fillStyle = '#fff';
                        ctx.fillText(problem.name, x, y);

                        // My best
                        ctx.beginPath();
                        x = idx * pxPerWidth;
                        y = pxPerHeight * (max - problem.score);
                        ctx.arc(x, y, 7, 0, Math.PI * 2);
                        ctx.fillStyle = '#0078d4'
                        ctx.fill();
                        ctx.fillStyle = '#fff';
                        ctx.fillText(problem.name, x, y);

                        // Percentages
                        ctx.beginPath();
                        x = idx * pxPerWidth;
                        y = canvas.height * (1 - problem.percentage / 100); // pxPerHeight * (max * problem.percentage);
                        ctx.arc(x, y, 6, 0, Math.PI * 2);
                        ctx.fillStyle = '#F75C03'
                        ctx.fill();
                        ctx.fillStyle = '#fff';
                        ctx.fillText(problem.name, x, y);
                    });

                });
            }
        }
    }
}

// @ts-ignore
const vm = new Vue(vueConfig);